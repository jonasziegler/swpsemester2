package at.zie.cashMachine;

import java.util.Scanner;

public class CashMachine {
	int balance = 100;
	int deposit;
	int withdraw;

	Scanner s = new Scanner(System.in);
	
	public void  startCashMachine() {
		
		System.out.println("Willkommen bei Ihrer Bank, w�hlen Sie eine Aktion aus: ");
		System.out.println("1. Einzahlung");
		System.out.println("2. Auszahlung");
		System.out.println("3. Kontostand");
		System.out.println("4. Ende");
		
		while (true) {
		
			int operation = s.nextInt();
			
			if(operation == 1) {
				System.out.println("Wie viel wollen Sie einzahlen?");
				deposit = s.nextInt();
				balance = balance + deposit;
				System.out.println("Ihr Geld wurde eingezahlt");
				System.out.println("Ihr neuer Kontostand betr�gt: " + balance);
				System.out.println("");
				
				
			}else if(operation == 2) {
				System.out.println("Wie viel wollen Sie abheben?");
				withdraw = s.nextInt();
				if(balance >= withdraw) {
					balance = balance - withdraw;
					System.out.println("Ihr Geld wurde ausgezahlt");
					System.out.println("Ihr neuer Kontostand betr�gt: " + balance);
				}else {
					System.out.println("Sie sind zu arm um diesen Betrag aus zu zahlen!");
				}
				System.out.println("");
				
			}else if(operation == 3) {
				System.out.println("Ihr Kontostand: " + balance);
				System.out.println("");
				
				
			}else if(operation == 4) {
				System.out.println("tsch�ss");
				System.exit(0);
			}else {
				System.out.println("Diese Aktion ist unbekannt");
			}
		}
	}
}
