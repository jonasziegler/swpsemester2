package at.zie.vierGewinnt;

import java.util.Scanner;

public class vierGewinnt {
	Scanner s = new Scanner(System.in);
	char player = 'X';
	char[][] field = new char[6][7];
	boolean winner = false;
	int currentRow = 0;
	
	private void init() {
		for (int i = 0; i < field.length; i++) { //i=col
			for (int j = 0; j < (field.length-1); j++) { //j=row
				field[i][j] = 0;
			}
		}
	}
	
	private void showBoard() {
		for (int col = 0; col < field.length; col++) {
			String currentRow = " | ";
			for (int row = 0; row < field.length+1; row++) {
				currentRow += field[col][row] + " | ";
			}
			System.out.println(currentRow);
		}
	}

	
	private int getInput() {

		System.out.println("Spieler " + player + " w�hle eine Spalte");
		String input = s.next();
		int in = Integer.parseInt(input);
		return in;

	}

	private boolean addInputToBoard(int input) {
		try {
			currentRow = 0;
			int column = input;
			int row = field.length-1;
			boolean setPlayer = false;
			
			if(field[row][column] >0) {
				for (int i = 0; i < field.length; i++) {
					if(setPlayer) {
						i = row;
					}else {	
						if(field[row-i][column] > 0) {
							setPlayer = false;
						}else {
							field[row-i][column] = player;
							setPlayer = true;
							currentRow = (row-i);
						}
					}
				}
				return setPlayer;
			}else {
				field[row][column] = player;
				currentRow = row;
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array out of Bounds");
			return false;
		} catch (NumberFormatException e) {
			System.out.println("Number Format");
			return false;
		}

	}

	private void changePlayer() {
		if (player == 'X') {
			player = 'O';
		} else if (player == 'O') {
			player = 'X';
		}
	}

	private boolean checkForWinner(int inputCol) {
		boolean theWinner = false;
		int connected = 1;
		
		int currentX = inputCol;
		int currentY = currentRow;
		//horizontal
		try {
			for (int i = 1; field[currentY][currentX+i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentY][currentX-i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		if(connected >= 4) theWinner = true;
	
		connected =1;
		
		//vertikal
		try {
			for (int i = 1; field[currentY+i][currentX] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentY-i][currentX] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		if(connected >= 4) theWinner = true;
		connected =1;
		
		//diagonal1
		try {
			for (int i = 1; field[currentY+i][currentX+i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentY-i][currentX-i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		if(connected >= 4) theWinner = true;
		connected =1;
		
		//diagonal2
		 try {
			for (int i = 1; field[currentY+i][currentX-i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentY-i][currentX+i] == player; i++) {
				connected++;
			}
		}catch (ArrayIndexOutOfBoundsException e) {}
		if(connected >= 4) theWinner = true;
		 
		System.out.println("connected: " + connected);
		return theWinner;
	}


	public void startGame() {
		init();
		while (!winner) {
			showBoard();
			int input = getInput();
			boolean success = addInputToBoard(input);
			
			if (success) {
				boolean someWinner = checkForWinner(input);
				if (someWinner) {
					showBoard();
					System.out.println("Spieler: " + player + " hat gewonnen!");
					winner = true;
				}
				changePlayer();
			}
			
		}
	}
}
