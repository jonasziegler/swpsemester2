package at.zie.encript;

public class CesarEncryption implements Encripter{
	
	public int shift;
	
	
	
	public CesarEncryption() {
		super();
		this.shift = 2;
	}

	public CesarEncryption(int shift) {
		super();
		this.shift = shift;
		if(this.shift == 0) {
			this.shift = 2;
		}
	}

	@Override
	public String encript(String data) {
		// TODO Auto-generated method stub
		String result = "";
		
		for (int i = 0; i < data.length(); i++) {
			
			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii +shift);
				
			if(ascii >= 65 && ascii <= 90) {
				if (shifted > 90) {
					shifted -= 26; //gro� Z
					
				}
				result += shifted;
			}else if (ascii >= 97 && ascii <= 122 ) {
				if(shifted > 122) {
					 shifted -= 26; //klein z
				}
				result += shifted;
			}else {
				result += character;
			}
							 
		}
		
		return result;
	}

	@Override
	public String decript(String data) {
		// TODO Auto-generated method stub
String result = "";
		
		for (int i = 0; i < data.length(); i++) {
			
			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii -shift);
				
			if(ascii >= 65 && ascii <= 90) {
				if (shifted < 65) {
					shifted += 26; //gro� A
					
				}
				result += shifted;
			}else if (ascii >= 97 && ascii <= 122 ) {
				if(shifted < 97 ) {
					 shifted += 26; //klein a
				}
				result += shifted;
			}else {
				result += character;
			}
							 		
		}
		
		return result;
	}

}
